insert into driver (first_name, last_name, telephone, experience, age, is_there_a_drivers_license, rights_category,
                    rating)
values ('Sergey', 'Suhachev',89172184533,5,40,true,'BC',4.1);
insert into driver (first_name, last_name, telephone, experience, age, is_there_a_drivers_license, rights_category,
                    rating)
values ('Ilya', 'Bukanov',89273884165,11,27,true,'ABCD',4.9);
insert into driver (first_name, last_name, telephone, experience, age, is_there_a_drivers_license, rights_category,
                    rating)
values ('Elena', 'Gerberova',89114562218,7,31,true,'AB',4.4);
insert into driver (first_name, last_name, telephone, experience, age, is_there_a_drivers_license, rights_category,
                    rating)
values ('Dima', 'Pakeev',89174550531,1,36,false,'BC',1.2);
insert into driver (first_name, last_name, telephone, experience, age, is_there_a_drivers_license, rights_category,
                    rating)
values ('Sveta', 'Oskina',89172365577,2,22,true,'B',2.8);




insert into car (model, color, number, Owner_ID)
values ('Vaz2106', 'blak','A666AA96',1);
insert into car (model, color, number, Owner_ID)
values ('AudiA100', 'red','C555OН95',2);
insert into car (model, color, number, Owner_ID)
values ('Vaz2109', 'white','В136ГР96',1);
insert into car (model, color, number, Owner_ID)
values ('BMWx6', 'blue','Б777ОГ95',2);
insert into car (model, color, number, Owner_ID)
values ('RENO', 'green','Н824ОВ84',3);




insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (1,1,'2022-11-19 04:35:00','01:35');
insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (5,3,'2022-11-21 12:00:00','03:00');
insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (2,2,'2022-11-21 18:45:00','02:13');
insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (3,5,'2022-11-22 14:35:00','00:35');
insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (3,4,'2022-11-22 18:20:00','04:55');
insert into trip (driver_id, car_id, Date_of_the_trip, Duration_of_the_trip)
values (4,1,'2022-11-24 00:05:00','01:20');






