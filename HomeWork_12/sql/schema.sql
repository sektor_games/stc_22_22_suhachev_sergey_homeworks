drop table if exists driver;
create table driver(
                       id_driver bigserial primary key ,
                       first_name char(20) default 'DEFAULT_FIRSTNAME',
                       last_name  char(20) default 'DEFAULT_LASTNAME',
                       telephone char(11) default '***********',
                       experience integer,
                       age integer,
                       is_there_a_drivers_license bool,
                       rights_category char(4),
                       rating float check (rating >= 0 and rating <= 5)
);
drop table if exists car;
create  table  car (
                       id_car bigserial primary key ,
                       model char(20),
                       color char(15),
                       number char(9) default  'A000AA000',
                       Owner_ID integer not null ,
                       foreign key (Owner_ID) references driver(id_driver)
);
drop table if exists trip;
create table  trip (
                       driver_id integer not null ,
                       foreign key (driver_id) references driver(id_driver),
                       car_id integer not null,
                       foreign key (car_id) references car(id_car),
                       Date_of_the_trip timestamp,
                       Duration_of_the_trip time
);