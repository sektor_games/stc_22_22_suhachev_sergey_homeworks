import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static Random rand = new Random();
    public static int Sum(int a, int b) {
        if (a < b) {
            int s = 0;
            while (a <= b) {
                s += a;
                a++;
            }
            return s;
        } else return -1;
    }
    public static void Arrayeven(int[] ar) {
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] % 2 == 0) System.out.println(ar[i]);

        }
    }
    public static int[] GeneratorArray(int l,int bound) {
        int[] arr = new int[l];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(bound);
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    public static int toInt(int[] arr) {
        int l = arr.length;
        int result = 0;
        for (int i = 0; i < arr.length; i++) {
            result += arr[i] * Math.pow(10, l);
            l--;
        }
        return result/10;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("задача номер 1 ");
        System.out.println("Введите 2 любых целых числа");
        int n0 = scan.nextInt();
        int n1 = scan.nextInt();
        int sum = Sum(n0, n1);
        if (sum != -1) {
            System.out.println("Сумма всех чисел в заданом интервале " + sum);
        } else {
            System.out.println("первое число больше или равно второму " + sum);
        }
        System.out.println("задача номер 2 ");
        while (true) {
            System.out.println("Введите размер желаемого массива");
            int larray = scan.nextInt();
            if (larray > 0) {
                int[] array = GeneratorArray(larray,100);
                Arrayeven(array);
                break;
            } else System.out.println("Введите число больше 0");
        }
        System.out.println("задача номер 3 со звездочкой");
        while (true) {
            System.out.println("Введите размер желаемого массива");
            int larr = scan.nextInt();
            if (larr > 0) {
                int[] array = GeneratorArray(larr,10);
                System.out.println("*** Результат ***  " + toInt(array));
                break;
            } else System.out.println("Введите число больше 0");
        }
    }
}