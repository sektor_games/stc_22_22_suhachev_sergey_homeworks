import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MAN {
    String name;
    public static Random rand;
    public static int money = 0;
    public static ATM[] atms;
    public static int kolatams, thisatm;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        rand = new Random();
        System.out.println("Сколько хотите найти банкоматов ?");
        kolatams = scan.nextInt();
        searchatms(kolatams);
        while (true) {
            // infoatms();
            System.out.println("Выберите банкомат по его номеру или нажмите любое другое число для выхода ");
            infoatms(-1);
            int choice = scan.nextInt();

            if (choice >= 1 & choice <= kolatams) {
                while (true) {
                    thisatm = choice - 1;
                    System.out.println("Выберите действие с банкоматом  № " + choice);
                    System.out.println("1-снять деньги / 2-положить деньги / или нажмите любое другое число  для выхода   ");
                    int choice1 = scan.nextInt();
                    if (choice1 == 1) {
                        System.out.println("Введите желаемую сумму ");
                        int transin = atms[thisatm].issuance(scan.nextInt());
                        if (transin != -1) {
                            money += transin;
                        } else
                            System.out.println("Введите адекватную сумму в пределах мах выдачи и не больше сколько есть в наличии ");
                    } else if (choice1 == 2) {
                        System.out.println("Введите сумму которую хотите положить . ");
                        int transout = scan.nextInt();
                        if (transout <= money) {
                            money -= transout;
                            int trans = atms[thisatm].receipts(transout);
                            if (trans == -1) {
                                System.out.println("деньги положены все  ");

                            } else {
                                System.out.println("банкомат заполнен и вам вернули лишнее в размере   " + trans);
                                money += trans;
                            }


                        } else
                            System.out.println("Введите адекватную сумму в пределах мах выдачи и не прыващающию сколько есть в наличии ");

                    } else break;
                    System.out.println("сейчас у вас  " + money);
                    infoatms(thisatm);
                }
                System.out.println(" ");
            } else {
                break;
            }

        }
        System.out.println("я вас слышу по поводу неиспользования самодельных генираторов но пока что я не понимаю как делать по другому ");
    }

    public static void searchatms(int n) {
        kolatams = n;
        atms = new ATM[n];
        for (int i = 0; i < n; i++) {
            int maxissued = (1 + rand.nextInt(3)) * 10;
            int maxmoney = (rand.nextInt(3) + 1) * 100;
            atms[i] = new ATM(maxissued, maxmoney);
            atms[i].remainingmoney = 100;

        }

    }

    public static void infoatms(int t) {
        if (t == -1) {
            String[] names = new String[kolatams];
            String[] maxmoneys = new String[kolatams];
            String[] maxexit = new String[kolatams];
            String[] money = new String[kolatams];
            String[] trans = new String[kolatams];
            for (int i = 0; i < kolatams; i++) {
                names[i] = "Банкомат  № " + (i + 1) + "         .";
                maxmoneys[i] = "Макс. обьем " + atms[i].maxmoney + "       .";
                maxexit[i] = "Макс.выдача " + atms[i].maxissued + "        .";
                money[i] = "В наличии   " + atms[i].remainingmoney + "       .";
                trans[i] = "Трансакции  " + atms[i].numberoperation + "         .";
            }
            System.out.println(Arrays.toString(names));
            System.out.println(Arrays.toString(maxmoneys));
            System.out.println(Arrays.toString(maxexit));
            System.out.println(Arrays.toString(money));
            System.out.println(Arrays.toString(trans));
        }else{
            System.out.println("Банкомат  № " + (t + 1) + "         .");
            System.out.println("Макс. обьем " + atms[t].maxmoney + "       .");
            System.out.println("Макс.выдача " + atms[t].maxissued + "        .");
            System.out.println("В наличии   " + atms[t].remainingmoney + "       .");
            System.out.println("Трансакции  " + atms[t].numberoperation + "         .");
        }

    }
}
