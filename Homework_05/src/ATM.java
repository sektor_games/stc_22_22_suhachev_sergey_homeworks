public class ATM {

    int remainingmoney = 0;
    int maxissued = 0;
    int maxmoney = 0;
    int numberoperation = 0;

    ATM(int maxiss, int maxmon) {
        maxissued = maxiss;
        maxmoney = maxmon;
    }

    //поступление
    int receipts(int mon) {
        if (remainingmoney != maxmoney) {
            int sum = remainingmoney + mon;
            if (sum <= maxmoney) {
                remainingmoney = sum;
                numberoperation++;
                return -1;
            } else {
                remainingmoney = maxmoney;
                numberoperation++;
                return sum - maxmoney;
            }
        } else return mon;
    }

    //выдача
    int issuance(int mon) {
        if (mon <= maxissued & mon <= remainingmoney) {
            remainingmoney -= mon;
            numberoperation++;
            return mon;
        } else return -1;
    }


}
