import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        while (true) {
            System.out.println("Введите размер желаемого массива");
            int larray = scan.nextInt();
            if (larray > 1) {
                int[] array = new int[larray];
                for (int i = 0; i < array.length; i++) {
                    array[i] = rand.nextInt(100);
                }
                int kolmin = 0;
                System.out.println(Arrays.toString(array));

                for (int i = 0; i < array.length; i++) {
                    if (i == 0) {
                        if (array[i] < array[i + 1]) kolmin++;
                    } else if (i == array.length - 1) {
                        if (array[i] < array[i - 1]) kolmin++;
                    } else {
                        if (array[i - 1] > array[i] & array[i] < array[i + 1]) kolmin++;
                    }
                }
                System.out.println("колличество локальных минимумов - " + kolmin);
                break;
            } else {
                System.out.println("размер массива должен быть больше 1 ");
            }
        }
    }
}