public class Main {
    public static void main(String[] args) {
        EvenNumbersPrintTask evennumber = new EvenNumbersPrintTask(-5, 5);
        OddNumbersPrintTask oddnumber = new OddNumbersPrintTask(5, 10);
        Tack[] tacks = {evennumber, oddnumber};
       completeAllTaks(tacks);

    }

    public static void completeAllTaks(Tack[] tacks) {
        for (int i = 0; i < tacks.length; i++) {
            tacks[i].complete();

        }

    }
}