public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {
    public EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }
    public void complete() {
        System.out.println("вывод четных ");
        for (int i = from; i < to; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
}
