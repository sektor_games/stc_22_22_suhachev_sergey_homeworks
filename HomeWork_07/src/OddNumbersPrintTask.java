public class OddNumbersPrintTask extends AbstractNumbersPrintTask {
    public OddNumbersPrintTask(int from, int to) {
        super(from, to);

    }

    public void complete() {
        System.out.println("вывод нечетных ");
        for (int i = from; i < to; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }

    ;
}
