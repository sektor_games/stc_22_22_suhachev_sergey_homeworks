package ru.inno;

import java.util.Random;

public class Main {
   // такая реализация чисто для закрепления материала
    static List<Integer> integerList = new ArrayList<>(){{
        add(23);
        add(33);
        add(88);
        add(33);
        add(15);
        add(55);
    }};
   static List<String> stringList = new LinkedList<>();
    public static void main(String[] args) {
        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("PHP!");
        stringList.add("Cobol!");
        info(integerList);
        integerList.remove(33);
        integerList.removeAt(3);
        assert 4== integerList.size();
        assert integerList.contains(33);
        assert !integerList.contains(15);
        info(integerList);
        info(stringList);
        stringList.remove("Bye!");
        stringList.removeAt(4);
        assert 4== stringList.size();
        assert !stringList.contains("Bye!");
        assert !stringList.contains("Cobol!");
        info(stringList);

    }
    // такая реализация чисто для закрепления материала
    static   void info(List list ){
        System.out.println(" лист длиною "+list.size());
        System.out.println("------------------------------------------");
        for (int i = 0; i < list.size(); i++) {
           System.out.println(list.get(i));
        }
        System.out.println("------------------------------------------");
    }
}