package ru.inno;

import nodes.Node;

public class LinkedList<T> implements List<T> {

    private Node<T> first;

    private Node<T> last;
    private int count;

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }

    @Override
    public void remove(T element) {
        Node<T> current = this.first;
        Node<T> currentold = this.first;
        boolean offset = false;
        // пока не обошли весь список
        while (current != null) {
            // если значение в текущем узле совпало с искомым

            if (current.value.equals(element)) {
                offset = true;
            }
            if (offset) {

                if (current.next != null) {
                    current.value = current.next.value;
                } else {
                    this.last = currentold;
                    currentold.next = null;
                }
            }
            currentold = current;
            // если не совпало - идем к следующему узлу
            current = current.next;

        }
        if (offset) count--;
    }

    @Override
    public void add(T element) {
        // создаем новый узел со значением
        Node<T> newNode = new Node<>(element);
        // если элементов в списке нет
        if (count == 0) {
            // кладем новый узел в качестве первого и последнего
            this.first = newNode;
        } else {
            // если в списке уже есть узлы
            // у последнего делаем следующим новый узел
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }


    @Override
    public boolean contains(T element) {
        // получить ссылку на первый элемент списка
        // пройтись по всем элементам списка
        Node<T> current = this.first;
        // пока не обошли весь список
        while (current != null) {
            // если значение в текущем узле совпало с искомым
            if (current.value.equals(element)) {
                return true;
            }
            // если не совпало - идем к следующему узлу
            current = current.next;
        }
        // если не нашли элемент
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeAt(int index) {
        if (0 <= index && index < count) {
            // начинаем с первого элемента
            Node<T> current = this.first;
            Node<T> currentold = null;
            boolean offset = false;
            for (int i = 0; i < count; i++) {
                if (i == index) {
                    offset = true;
                    count--;
                }
                if (offset) {
                    if (current.next != null) {
                        current.value = current.next.value;
                    }
                    if (i == count) {
                        currentold.next = null;
                        this.last = currentold;
                    }
                }
                currentold = current;
                current = current.next;
            }
        }
    }



    @Override
    public T get(int index) {
        if (0 <= index && index < count) {
            // начинаем с первого элемента
            Node<T> current = this.first;

            // если запрошенный индекс был равен - 3
            // 0, 1, 2
            for (int i = 0; i < index; i++) {
                // на каждом шаге цикла двигаемся дальше
                current = current.next;
            }
            return current.value;
        }
        return null;

    }
}
