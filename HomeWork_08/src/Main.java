public class Main {
    public static void main(String[] args) {
        ArraysTasksResolver arraysTasksResolver = new ArraysTasksResolver();
        int[] arr = {12, 64, 2, 4, 88, 40, 56};

        ArrayTask arrayTask1 = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i < to; i++) {
                sum += array[i];
            }
            return sum;
        };

        ArrayTask arrayTask2 = (array, from, to) -> {
            int maxnum = array[from];
            for (int i = from; i < to; i++) {
               if(maxnum<array[i])maxnum=array[i];
            }
           int  sum = maxnum%10+(int) maxnum/10;
            return  sum;

        };
        ArraysTasksResolver.resolveTask(arr, arrayTask1, 0, 4);
        ArraysTasksResolver.resolveTask(arr, arrayTask2, 2, 6);
        // честно я пытаюсь понять как проверять через асерт но я не как не могу понять как в данном случае проверять ?
        assert 4 == 2+4;
        // и в чем тут логика почему ошибку не фиксирует ?

    }
}