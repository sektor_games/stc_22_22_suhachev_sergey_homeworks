import java.util.ArrayList;
import java.util.List;
import java.io.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepostory {


    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }
    private static final Function<String, Product> stringToProductMapper = currentLine -> {
        String[] parts = currentLine.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Float price = Float.parseFloat(parts[2]);
        Integer count = Integer.parseInt(parts[3]);
        return new Product(id, name, price, count);
    };
    private static final Function<Product, String> productToStringMapper = currentProduct ->
            currentProduct.getName() + "|" + currentProduct.getPrice() + "|" + currentProduct.getCount();

    public List<Product> findAll() {
        try {
           return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
    public Product findById(Integer id) {
        List<Product> products = findAll();
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).getId()==id){
                return products.get(i);
            }
        }
        return null;
    }

    public List<Product> findAllByTitleLike(String title) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getName().toLowerCase().contains(title.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public void update(Product product) {
        List<Product> products = findAll();
        for (int i = 0; i < products.size(); i++) {
            if(products.get(i).equals(product)){
                products.get(i).setCount(product.getCount());
                products.get(i).setPrice(product.getPrice());
                break;
            }
        }
        try (FileWriter fileWriter = new FileWriter(fileName);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (int i = 0; i < products.size(); i++) {
                String currentLine = (i + 1) + "|" + productToStringMapper.apply(products.get(i));
                bufferedWriter.write(currentLine);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
}
