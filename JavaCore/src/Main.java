public class Main {
    public static void main(String[] args) {
        ProductsRepostory productsRepostory = new ProductsRepositoryFileBasedImpl("Склад");
        System.out.println(productsRepostory.findAllByTitleLike("оло"));
        System.out.println(productsRepostory.findAllByTitleLike("си"));
        Product milk=productsRepostory.findById(1);
        Product vodka=productsRepostory.findById(6);
        milk.setCount(100);
        milk.setPrice(40.1f);
        productsRepostory.update(milk);
        vodka.setCount(1000);
        vodka.setPrice(400.1f);
        productsRepostory.update(vodka);
//         сори я не придумал как это проверить через assert
    }
}