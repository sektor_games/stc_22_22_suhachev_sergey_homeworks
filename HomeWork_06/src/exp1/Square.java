package exp1;

public class Square extends Shape {
    private float side;

    public Square(String name,float side) {
        super(name);
        this.side=side;

    }

    public void setSide(float side) {
        this.side = side;
    }
    public float getSide() {
        return side;
    }
    public float area(){
         return side*side;
    }
    public  float perimeter(){
        return (side + side)*2;
    }
}
