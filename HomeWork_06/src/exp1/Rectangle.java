package exp1;

public class Rectangle extends  Square  {
    private float sideB;

    public Rectangle(String name,float side,float sideB) {
        super(name,side);
        this.sideB=sideB;
    }
    public float area(){
        return super.getSide()*sideB;
    }
    public  float perimeter(){
        return (super.getSide() + sideB)*2;
    }
}
