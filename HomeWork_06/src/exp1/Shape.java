package exp1;

public class Shape {
    private String  name ;
    private float positionX;
    private float positionY;

    public Shape(String name) {
        this.name = name;
    }
     public String getName() {
        return name;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }
    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }
    public  float getPositionX(){
        return  positionX;
    }
    public  float getPositionY(){
        return  positionY;
    }
    public  void move(float x,float y){
        positionX=x;
        positionY=y;
    }


}
