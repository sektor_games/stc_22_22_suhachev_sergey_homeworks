package exp1;

public class Ellipse extends  Circle{
    private float radiusB;
    public Ellipse(String name,float radius,float radiusB) {
        super(name,radius);
        this.radiusB= radiusB;
    }

    public float getRadiusB() {
        return radiusB;
    }

    public void setRadiusB(float radiusB) {
        this.radiusB = radiusB;
    }
    public float area(){
        return  (float) Math.PI * (super.getRadius() * radiusB);
    }
    public  float perimeter(){
        return  (float)(2 * Math.PI  * Math.sqrt((super.getRadius()  * super.getRadius()+ radiusB * radiusB) / 2)) ;






    }
}
