package exp1;

public class Circle extends Shape {
    private  float radius;
    public Circle(String name,float radius) {
        super(name);
        this.radius=radius;
    }
    public float getRadius() {
        return radius;
    }
    public void setRadius(float radius) {
        this.radius = radius;
    }
    public float area(){
        return  (float) Math.PI * (radius * radius);
    }
    public  float perimeter(){
        return (float) Math.PI * 2*radius;
    }
}
